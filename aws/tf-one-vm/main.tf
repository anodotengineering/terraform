terraform {
  backend "s3" {
    bucket = "anodot-terraform"

    ## for multi account configuration key should be defined explicitly. Usualy it should be
    # key  = "dm-staging"
    # key  = "dm-production"
    region = "us-east-1"

    profile = "default"

    //    role_arn = "arn:aws:iam::915728697716:role/fdna-dm-terraform-access"
  }
}

provider "aws" {
  region = "${var.aws_region}"

  profile = "default"

  //  assume_role {
  //    role_arn     = "${var.environment_iam_roles}"
  //  }
}
