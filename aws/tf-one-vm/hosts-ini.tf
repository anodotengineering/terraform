data "template_file" "inventory" {
    template = templatefile("${path.module}/templates/hosts-ini.tpl", { node_ip = aws_instance.k8s_host.private_ip })
}

resource "local_file" "hosts-ini" {
    content  = "${data.template_file.inventory.rendered}"
    filename = "${path.module}/hosts.ini"
}

resource "local_file" "host-txt" {
    content     = "${aws_instance.k8s_host.public_ip}"
    filename    = "${path.module}/HOST.txt"
}

resource "local_file" "privatipe-txt" {
    content     = "${aws_instance.k8s_host.private_ip}"
    filename    = "${path.module}/private_ip.txt"
}
